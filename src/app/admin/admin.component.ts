import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  showHomeNavBar:boolean=false;
  showAdminNavBar:boolean=true;

  constructor() { }

  ngOnInit() {
    this.showHomeNavBar=false;
  	this.showAdminNavBar=true;
  }

}
