import { Component } from '@angular/core';

@Component({
  selector: 'admin-header',
  templateUrl: './admin.header.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminHeaderComponent {
  showHomeNavBar:boolean=false;
  showAdminNavBar:boolean=true;
}
