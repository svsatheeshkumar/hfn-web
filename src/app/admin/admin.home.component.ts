import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.home.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminHomeComponent implements OnInit {

  showHomeNavBar:boolean=false;
  showAdminNavBar:boolean=true;

  constructor() { }

  ngOnInit() {
  	this.showHomeNavBar = false;
  	this.showAdminNavBar = true;
  }

}
