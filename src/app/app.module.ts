import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { ModalModule } from 'ngx-bootstrap/modal';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService }  from './in-memory-data.service';
import {APP_BASE_HREF, LocationStrategy, HashLocationStrategy} from '@angular/common'

import { AppComponent } from './app.component';
import { AppHeaderComponent } from './app.header.component';
import { AppFooterComponent } from './app.footer.component';
import { AdminHeaderComponent } from './admin/admin.header.component';
import { LoginComponent } from './login/login.component';
import { RegistrationComponent } from './registration/registration.component';
import { LoginService } from './login/service/login.service';
import { LocationService } from './location/service/location.service';
import { UserService } from './user/service/user.service';
import { UserAttendanceService } from './user/service/user.attendance.service';
import { AppRoutingModule } from './app-routing.module';
import { LocationComponent } from './location/location.component';
import { UserComponent } from './user/user.component';
import { UserAttendanceComponent } from './user/user.attendance.component';
import { AdminComponent } from './admin/admin.component';
import { AdminHomeComponent } from './admin/admin.home.component';

@NgModule({
  declarations: [
    AppComponent,
    AppHeaderComponent,
    AppFooterComponent,
    AdminHeaderComponent,
    LoginComponent,
    RegistrationComponent,
    LocationComponent,
    UserComponent,
    UserAttendanceComponent,
    AdminComponent,
    AdminHomeComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    BsDropdownModule.forRoot(),
    TooltipModule.forRoot(),
    ModalModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,

    // The HttpClientInMemoryWebApiModule module intercepts HTTP requests
    // and returns simulated server responses.
    // Remove it when a real server is ready to receive requests.
   /* HttpClientInMemoryWebApiModule.forRoot(
      InMemoryDataService, { dataEncapsulation: false }
    )*/
  ],
  providers: [
    {provide: APP_BASE_HREF, useValue: '/hfnweb/'},
    LoginService,
    LocationService,
    UserService,
    UserAttendanceService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
