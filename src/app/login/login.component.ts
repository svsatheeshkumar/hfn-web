import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject }    from 'rxjs/Subject';
import { of }         from 'rxjs/observable/of';
import { Router } from "@angular/router";
import { FormsModule } from '@angular/forms';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';

import { LoginService } from './service/login.service';
import { Location } from '../location/model/location';
import { LocationService } from '../location/service/location.service';
import { User } from '../user/model/user';
import { UserService } from '../user/service/user.service';
import { UserAttendance } from '../user/model/user.attendance';
import { UserAttendanceService } from '../user/service/user.attendance.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {

  users$: Observable<User[]>;
  private searchTerms = new Subject<string>();
  private defaultUser = {id:-1, hfnId:null, name:null, email:null, phoneNumber:null, locationId:-1};

  showHomeNavBar:boolean=true;
  showAdminNavBar:boolean=false;

  location:Location;
  locations: Location[];
  userList: User[];
  user:User = this.defaultUser;
  flag: boolean = true;
  attendanceSuccess: boolean = false;
  userAttendanceList: UserAttendance[];
  hfnIdRequired: boolean = false;
  locationRequired: boolean = false;

  constructor(private locationService: LocationService,
              private userService: UserService,
              private userAttendanceService: UserAttendanceService,
              private router: Router) { }

  getLocations(): void {
    this.locationService.getLocations()
       .subscribe(locations => this.locations = locations);
  }

  getUsers(): void {
    this.userService.getUsers()
       .subscribe(users => this.userList = users);
  }

  getUserAttendances(): void {
    this.userAttendanceService.getUserAttendances()
       .subscribe(usrAttendances => this.userAttendanceList = usrAttendances);
  }

  reqHfnId(){
    if(this.user && this.user.id !== -1){
      if(this.user.hfnId === ""){
         this.hfnIdRequired = true;
       }else{
         this.hfnIdRequired = false;
       }
    }
  }

 reqLocation(){
   if(this.user && this.user.id !== -1){
      if(""+this.user.locationId === "-1"){
          this.locationRequired = true;
       }else{
         this.locationRequired = false;
       }
     }
  }

  validForm(){
    if(this.hfnIdRequired || this.locationRequired){
         return false;
    }
    return true;
  }

  onConfirm(){
    if(this.user && this.user.id !== -1){
     
     if(!this.validForm()){
       return false;
     }

     let userAttendance = {
        id : Math.floor(Math.random() * 200),
        userId : this.user.id,
        hfnId:this.user.hfnId,
        locationId: this.user.locationId
      }
     
      this.userAttendanceService.addUserAttendance(userAttendance)
      .subscribe(usr => {
        this.getUserAttendances();
        this.attendanceSuccess = true;
      });

    }else{
      //Redirect to registration page
       if(this.user && this.user.name && this.user.name !== ""){
          this.router.navigate(['/registration',this.user.name]);  
       }
    }
  }

  // Push a search term into the observable stream.
  search(term: string): void {
    this.attendanceSuccess = false;
    this.flag = true;
    if(term === ""){
        this.user = this.defaultUser;
    }else{
      this.searchTerms.next(term);
    }
  }

  reset():void{
    this.attendanceSuccess = false;
    this.user = this.defaultUser;
  }

  onSelectUser(selectedUser) {
    if (selectedUser) {
      this.user = selectedUser;
      this.flag = false;
    }else {
      return false;
    }
  }

  ngOnInit() {
    this.showHomeNavBar=true;
    this.showAdminNavBar=false;
    this.getLocations();
    this.getUsers();
    this.getUserAttendances();

    this.users$ = this.searchTerms.pipe(

        // wait 300ms after each keystroke before considering the term
        debounceTime(300),

        // ignore new term if same as previous term
        distinctUntilChanged(),

        // switch to new search observable each time the term changes
        switchMap((term: string) => this.userService.searchUsers(term)),
      );
    }
}


