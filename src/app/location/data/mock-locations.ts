import { Location } from '../model/location';

export const LOCATIONS: Location[] = [
  { id: 11, name: 'NUS' },
  { id: 12, name: 'NTU' },
  { id: 13, name: 'A-Star' },
  { id: 14, name: 'Credit Suisse' },
  { id: 15, name: 'JP Morgan' },
  { id: 16, name: 'NTU' },
  { id: 17, name: 'Nomura' },
  { id: 18, name: 'Bank of America' },
  { id: 19, name: 'UOB' },
  { id: 20, name: 'SBI' }
];
