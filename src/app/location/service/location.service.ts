import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';

import { Location } from '../model/location';
import { LOCATIONS } from '../data/mock-locations';

@Injectable()
export class LocationService {


  private LOCATION_API_URL = 'http://localhost:8080/api/location';  

  constructor(private httpClient: HttpClient) { }

  getLocations(): Observable<Location[]> {
    //return of(LOCATIONS);

    return this.httpClient.get<Location[]>(this.LOCATION_API_URL+"/list")
    .pipe(
       catchError(this.handleError<Location[]>('getLocations'))
    )
  }


   /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      //this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
