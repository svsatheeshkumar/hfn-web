import { Component, OnInit, OnDestroy } from '@angular/core';
import { Location } from '../location/model/location';
import { LocationService } from '../location/service/location.service';

@Component({
  selector: 'app-location',
  templateUrl: './location.component.html',
  styleUrls: ['./location.component.css']
})
export class LocationComponent implements OnInit {

    locationSub : any;
	locations: Location[];

	constructor(private locationService: LocationService) {}


	getLocations(): void {
	    this.locationSub = this.locationService.getLocations()
	       .subscribe(locations => this.locations = locations);
	}

	ngOnInit() {
    	this.getLocations();
	}

	ngOnDestroy() {
	    this.locationSub.unsubscribe();
	}

}
