import { Component, OnInit, OnDestroy } from '@angular/core';

import { User } from './model/user';
import { UserService } from './service/user.service';
import { Location } from '../location/model/location';
import { LocationService } from '../location/service/location.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

	locationSub : any;
	userSub : any;
	users: User[];
	locations: Location[];

	constructor(private locationService: LocationService,
		        private userService: UserService) { 
		
	}

	getUsers(): void {
	   this.userSub =  this.userService.getUsers()
	       .subscribe(users => {
	       		//this.locationName = this.books.filter(book => book.store_id === this.store.id);
	       		this.users = users;
	       });
	}

	getLocations(): void {
	    this.locationSub = this.locationService.getLocations()
	       .subscribe(locations => this.locations = locations);
	}

    ngOnInit() {
    	this.getLocations();
    	this.getUsers();
	}

	ngOnDestroy() {
	    this.locationSub.unsubscribe();
	    this.userSub.unsubscribe();
	}

}
