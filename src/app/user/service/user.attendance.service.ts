import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';

import { UserAttendance } from '../model/user.attendance';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class UserAttendanceService {

  private USER_ATTANDANCE_URL = 'http://localhost:8080/api/userAttendance';

  constructor(private http: HttpClient) { }

  getUserAttendances(): Observable<UserAttendance[]> {
      return this.http.get<UserAttendance[]>(this.USER_ATTANDANCE_URL+"/list")
      .pipe(
      catchError(this.handleError('getUserAttendances', []))
    );
  }

  /** POST: add a new user to the server */
  addUserAttendance (userAttandance: UserAttendance): Observable<UserAttendance> {
    return this.http.post<UserAttendance>(this.USER_ATTANDANCE_URL+"/save", userAttandance, httpOptions)
    .pipe(
      catchError(this.handleError<UserAttendance>('addUserAttendance'))
    );
  }
  
  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      //this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

}
