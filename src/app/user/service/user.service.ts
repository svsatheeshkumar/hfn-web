import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';

import { User } from '../model/user';
import { USERS } from '../data/mock-users';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class UserService {

  private usersUrl = 'http://localhost:8080/api/user';


  constructor(private httpClient: HttpClient) { }

  getUsers(): Observable<User[]> {
    return this.httpClient.get<User[]>(this.usersUrl+"/list")
    .pipe(
       catchError(this.handleError<User[]>('getUsers'))
    )
  }

  addUser (user: User): Observable<User> {
    return this.httpClient.post<User>(this.usersUrl+"/save", user, httpOptions)
    .pipe(
      catchError(this.handleError<User>('addUser'))
    );
  }

  searchUsers(searchText: string): Observable<User[]> {
    if (!searchText.trim()) {
      return of([]);
    }

     // Initialize Params Object
    let searchParams = new HttpParams();

    // Begin assigning parameters
    searchParams = searchParams.append('searchText', searchText);
    return this.httpClient.get<User[]>(this.usersUrl+"/search",{params:searchParams})
    .pipe(
      catchError(this.handleError<User[]>('searchUsers', []))
    );

  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      //this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

}
