import { User } from '../model/user';

export const USERS: User[] = [
	{ id: 11, hfnId:"HFN0001", name: 'Satheesh', email:'sathi.nair@gmail.com', phoneNumber:'989898988',locationId:11 },
	{ id: 12, hfnId:"HFN0002", name: 'Lakshmikanth', email:'kanth@gmail.com', phoneNumber:'989898988',locationId:12 },
	{ id: 13, hfnId:"HFN0003", name: 'Sunil', email:'sunil@gmail.com', phoneNumber:'989898988',locationId:13  },
	{ id: 14, hfnId:"HFN0004", name: 'Manu', email:'manu@gmail.com', phoneNumber:'989898988',locationId:14  },
	{ id: 15, hfnId:"HFN0005", name: 'Vinod', email:'vinod@gmail.com', phoneNumber:'989898988',locationId:15  }
];
