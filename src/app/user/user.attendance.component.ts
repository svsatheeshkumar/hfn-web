import { Component, OnInit, OnDestroy } from '@angular/core';

import { User } from './model/user';
import { UserAttendance } from './model/user.attendance';
import { UserAttendanceService } from './service/user.attendance.service';
import { UserService } from './service/user.service';
import { Location } from '../location/model/location';
import { LocationService } from '../location/service/location.service';

@Component({
  selector: 'app-admin',
  templateUrl: './user.attendance.component.html',
  styleUrls: ['./user.component.css']
})
export class UserAttendanceComponent implements OnInit {

	locationSub : any;
	userSub : any;
	userAttendanceSub:any;
	users: User[];
	userAttendances: UserAttendance[];
	locations: Location[];

	constructor(private locationService: LocationService,
		        private userService: UserService,
		        private userAttendanceService: UserAttendanceService) { 
		
	}

   getUserAttendance(): void {
	   this.userAttendanceSub =  this.userAttendanceService.getUserAttendances()
	       .subscribe(usrAttendances => {
	       		//this.locationName = this.books.filter(book => book.store_id === this.store.id);
	       		this.userAttendances = usrAttendances;
	       });
	}

	getUsers(): void {
	   this.userSub =  this.userService.getUsers()
	       .subscribe(users => {
	       		//this.locationName = this.books.filter(book => book.store_id === this.store.id);
	       		this.users = users;
	       });
	}

	getLocations(): void {
	    this.locationSub = this.locationService.getLocations()
	       .subscribe(locations => this.locations = locations);
	}

    ngOnInit() {
    	this.getLocations();
    	this.getUsers();
    	this.getUserAttendance();
	}

	ngOnDestroy() {
	    this.locationSub.unsubscribe();
	    this.userSub.unsubscribe();
	    this.userAttendanceSub.unsubscribe();
	}

}
