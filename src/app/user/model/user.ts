export class User {
  id: number;
  hfnId: string;
  name: string;
  email: string;
  phoneNumber: string;
  locationId : number;
} 
