import { NgModule } from '@angular/core';
import { RouterModule, Routes} from '@angular/router';

import { LoginComponent }   from './login/login.component';
import { RegistrationComponent }  from './registration/registration.component';
import { UserComponent }  from './user/user.component';
import { UserAttendanceComponent }  from './user/user.attendance.component';
import { LocationComponent }  from './location/location.component';
import { AdminComponent }  from './admin/admin.component';
import { AdminHomeComponent }  from './admin/admin.home.component';

const routes: Routes = [
	{ path: '', redirectTo: '/login', pathMatch: 'full' },
	{ path: 'registration/:name', component: RegistrationComponent, pathMatch: 'full'},
	{ path: 'login', component: LoginComponent },
	{ path: 'user/list', component: UserComponent},
	{ path: 'location/list', component: LocationComponent },
	{ path: 'admin', component: AdminComponent, children: [
			{ path: 'home', component: AdminHomeComponent, outlet: 'app-admin' },
		    { path: 'userlist', component: UserComponent, outlet: 'app-admin' },
		    { path: 'userattendance', component: UserAttendanceComponent, outlet: 'app-admin' },
		    { path: ':locationlist', component: LocationComponent, outlet: 'app-admin' }
		  ]}
];

@NgModule({
  imports: [ RouterModule.forRoot(routes)],
  exports: [ RouterModule ]
})
export class AppRoutingModule { 
constructor() {
	
  }
}
