import { InMemoryDbService } from 'angular-in-memory-web-api';

export class InMemoryDataService implements InMemoryDbService {
createDb() {
    const users = [
      { id: 11, hfnId:"HFN0001", name: 'Satheesh', email:'sathi.nair@gmail.com', location:11 },
      { id: 12, hfnId:"HFN0002", name: 'Lakshmikanth', email:'kanth@gmail.com', location:12 },
      { id: 13, hfnId:"HFN0003", name: 'Sunil', email:'sunil@gmail.com', location:13 },
      { id: 14, hfnId:"HFN0004", name: 'Manu', email:'manu@gmail.com', location:14 },
      { id: 15, hfnId:"HFN0005", name: 'Vinod', email:'vinod@gmail.com', location:15 }
    ];

    const userAttendances = [];

    return {users, userAttendances};
  }
}
