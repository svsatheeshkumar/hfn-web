import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Router } from "@angular/router";
import { ActivatedRoute } from '@angular/router';

import { Location } from '../location/model/location';
import { LocationService } from '../location/service/location.service';
import { User } from '../user/model/user';
import { UserService } from '../user/service/user.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})

export class RegistrationComponent implements OnInit {

  private defaultUser = {id:null, hfnId:null, name:null, email:null, phoneNumber: null, locationId:null};
  private paramsSub: any;
  locations: Location[];
  user:User = this.defaultUser;
  userList: User[];
  userAddSuccess: boolean = false;

  constructor(private locationService: LocationService,
              private userService: UserService,
              private router: Router,
              private route: ActivatedRoute) { }


  onChangeLocation(event) {
      let value = event.target.value;

      if (value) {
          this.user.locationId = +value;
      }
  }

  onRegister(user:User) : void{
   // user.id = Math.floor(Math.random() * 200);
    this.userService.addUser(user)
    .subscribe(usr => {
      this.userAddSuccess = true;
    });

  }

  getUsers(): void {
    this.userService.getUsers()
       .subscribe(users => this.userList = users);
  }

  getLocations(): void {
    this.locationService.getLocations()
       .subscribe(locations => this.locations = locations);
  }

  goBackToLogin() : void{
    this.router.navigate(['/login']);
  }

  reset():void{
      this.userAddSuccess = false;
      this.user = this.defaultUser;
  }

  ngOnInit() {
    this.paramsSub = this.route.params.subscribe(params => {
       if(params['name'] !== 'null'){
          this.user.name = params['name'];
       }
    });

    this.getLocations();

  }

  ngOnDestroy() {
    this.paramsSub.unsubscribe();
  }

}
